package dtu.tokyo.task7.adapter.rs;

import java.util.Iterator;

import javax.ws.rs.BadRequestException;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.GET;

import dtu.tokyo.task7.logic.*;

@Path("/tokens")
public class TokensResource {
	public static TokenManager tokenManager = new TokenManager();
	
	@Path("/request")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response requestTokens(Customer cr, @QueryParam("number") int number) {
		try {
			//Boolean response = (Boolean) tokenManager.requestTokens(cr, number);
			String response = Boolean.toString( tokenManager.requestTokens(cr, number));
			
			response = response + " Token Numb. " + cr.getTokens().size() + "\n";
			
			for(Token t: cr.getTokens())
				response = response + "Token ID: " + t.getTokenID() + "\n";			
			
			return Response.status(201).entity(response).build();
		} catch (Exception e) {
			throw new BadRequestException(Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build());
		}
	}
	@Path("/use/{tokenid}")
	@POST
	@Consumes(MediaType.TEXT_PLAIN)
	@Produces(MediaType.TEXT_PLAIN)
	public boolean useToken(@PathParam("tokenid") String tokenid) {
		try {
			return TokensResource.tokenManager.useToken(new String(tokenid));
		} catch (Exception e) {
			throw new BadRequestException(Response.status(Response.Status.BAD_REQUEST).entity(e.getMessage()).build());
		}
	}

	@Path("/test")
	@GET
	@Produces("text/plain")
	public Response doGet(){
		return  Response.ok("Hello there!").build();
	}
}
