package  dtu.tokyo.task7.adapter.db.customer;

import java.util.HashMap;

import java.util.Vector;

import dtu.tokyo.task7.logic.Customer;
import dtu.tokyo.task7.logic.Token;

public class CustomerCollection implements CustomerRegister{
	HashMap<String, String> customerNames;
	HashMap<String, Vector<Token>> customerTokens;

	public CustomerCollection() {
		customerNames = new HashMap<>();
		customerTokens = new HashMap<>();
	}

	@Override
	public void storeCustomer(Customer c) {
		customerNames.put(c.getCustomerID(), c.getName());
		customerTokens.put(c.getCustomerID(), c.getTokens());
	}
	
	@Override
	public void updateCustomer(Customer c) {
		
		try {
			customerTokens.replace(c.getCustomerID(), c.getTokens());
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public boolean containsCustomer(String customerID) {
		return customerNames.containsKey(customerID);
	}
	
	@Override
	public Vector<Token> retrieveTokens(String customerID){
		return customerTokens.get(customerID);
	}

}
