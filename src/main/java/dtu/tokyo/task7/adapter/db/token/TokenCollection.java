package dtu.tokyo.task7.adapter.db.token;

import java.util.HashMap;

public class TokenCollection implements TokenRegister{
	HashMap<String, Boolean> tokens;
	
	public TokenCollection() {
		tokens = new HashMap<>();
	}
	
	@Override
	public void storeToken(String id) {
		tokens.put(id,true);
	}

	@Override
	public boolean getStatusOfToken(String tokenId) {
		return tokens.get(tokenId);
	}

	@Override
	public boolean containsToken(String tokenID) {
		return tokens.containsKey(tokenID);
	}

	@Override
	public void setStatusOfToken(String tokenID, boolean b) {
		tokens.put(tokenID, b);		
	}
}
