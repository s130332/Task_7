package dtu.tokyo.task7.adapter.db.customer;

import java.util.Vector;
import dtu.tokyo.task7.logic.*;

public interface CustomerRegister {
	public void storeCustomer(Customer customer);
	public void updateCustomer(Customer customer);
	public boolean containsCustomer(String customerID);
	public Vector<Token> retrieveTokens(String customerID); 
}
