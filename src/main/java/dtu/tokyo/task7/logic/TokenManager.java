package  dtu.tokyo.task7.logic;

import java.util.Iterator;


import dtu.tokyo.task7.adapter.db.customer.CustomerCollection;
import dtu.tokyo.task7.adapter.db.customer.CustomerRegister;
import dtu.tokyo.task7.adapter.db.token.TokenCollection;
import dtu.tokyo.task7.adapter.db.token.TokenRegister;

public class TokenManager {
	TokenRegister tokenRegister = new TokenCollection();
	CustomerRegister customerRegister = new CustomerCollection();

	public TokenManager() {
	}

	public boolean requestTokens(Customer c, int n) {
		boolean response=false;
		try {
		
			if(customerRegister.containsCustomer(c.getCustomerID())) {
				c.setTokens(customerRegister.retrieveTokens(c.getCustomerID()));
			}
			else {
				customerRegister.storeCustomer(c);
			}
		
		}
		catch (NullPointerException e){
			System.out.print("test");
			e.printStackTrace();
		}
		
		if(n > 0 && n <= 5) {
			Iterator<Token> ti=(Iterator<Token>) c.getTokens().iterator();
			int numberOfUnusedTokens = 0;
			while(ti.hasNext()) {
				Token t= (Token) ti.next();
				if(this.tokenRegister.getStatusOfToken(t.tokenID)) {
					numberOfUnusedTokens+=1;
				}
			}
			if(numberOfUnusedTokens < 2) {
				for(int i=0; i < n; i++) {
					Token t = new Token();
					c.getTokens().add(t);
					customerRegister.updateCustomer(c);
					tokenRegister.storeToken(t.tokenID);		
				}
				response = true;
			}
		}
		return response;
	}
	
	public boolean useToken(String tokenID) {
		boolean response=false;
		if(tokenRegister.containsToken(tokenID)&&tokenRegister.getStatusOfToken(tokenID)) {
			tokenRegister.setStatusOfToken(tokenID,false);
			response=true;
		}
		return response;
	}
	
	public TokenRegister getTokenRegister() {
		return tokenRegister;
	}
	public void setTokenRegister(TokenRegister tr) {
		tokenRegister= tr;
	}
	public CustomerRegister getCustomerRegister() {
		return customerRegister;
	}
	public void setCustomerRegister(CustomerRegister cr) {
		customerRegister=cr;
	}
}

